/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Modal } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

    state = {
        isModal: false
    }

    clickModal() {
        this.setState.isModal = true
    }

    hideModal() {
        this.setState.isModal = false
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>React-Native</Text>
                </View>

                <View style={styles.content}>

                    <View style={styles.row}>

                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>

                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>

                    </View>

                    <View style={styles.row}>
                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>

                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>

                        <View>
                            <Image
                                style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
                                source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
                                onPress={this.clickModal()}
                            />
                            <Modal onPress={this.hideModal()}>
                                <Image style={{
                                    width: '100%'
                                }} />
                            </Modal>
                        </View>

                    </View>

                </View>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'blue',

    },
    header: {
        alignItems: 'center',
    },
    headerText: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        padding: 20
    },
    content: {
        backgroundColor: 'yellow',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        backgroundColor: 'brown',
        flex: 1,
        margin: 14

    },
    box2: {
        backgroundColor: 'black',
        flex: 1,
        margin: 14,
    },
    row: {
        backgroundColor: 'lightblue',
        flex: 1,
        flexDirection: 'row'
    },
    txtcolor: {
        color: 'white',
        fontSize: 40,
    }
});
