/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>News</Text>
                </View>
                <View style={styles.content}>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                        <View style={styles.box2}>
                            <Text style={styles.txtcolor}>Lorems</Text>
                        </View>
                    </View>


                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'blue',

    },
    header: {
        alignItems: 'center',
    },
    headerText: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        padding: 20
    },
    content: {
        backgroundColor: 'yellow',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        backgroundColor: 'brown',
        flex: 1,
        margin: 14

    },
    box2: {
        backgroundColor: 'black',
        flex: 1,
        margin: 14,
    },
    row: {
        backgroundColor: 'lightblue',
        flex: 1,
        flexDirection: 'row'
    },
    txtcolor:{
        color:'white',
        fontSize:40,
    }
});
