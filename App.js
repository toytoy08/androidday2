/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Alert, TouchableOpacity, Image, TextInput } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {

  state = {
    username: '',
    password: ''
  }

  onValueChange = (field, value) => {
    this.setState({ [field]: value })
  }

  checkIdPass (){
      if (this.state.username == 'admin' && this.state.password == 'eiei')
      {
        Alert.alert('Right', 'You are True ADMIN')
      }
      else
      {
        Alert.alert('WRONG!!!!', 'You lie to me')
      }
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image
            style={{ width: 200, height: 200, borderRadius: 200 / 2 }}
            source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }}
          />
        </View>
        <View>
          <Text>{this.state.username} - {this.state.password}</Text>
          <TextInput style={styles.textin} onChangeText={value => { this.onValueChange("username", value) }} placeholder='TextInput'></TextInput>
          <TextInput style={styles.textin} onChangeText={value => { this.onValueChange("password", value) }} placeholder='TextInput'></TextInput>
        </View>
        <View>
          <TouchableOpacity onPress={this.checkIdPass}>
            <Text style={styles.button}>Text</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    alignItems: 'stretch',
    textAlign: 'center',
    width: 250
  },
  textin: {
    borderColor: 'gray',
    borderWidth: 1,
    width: 250,
    textAlign: 'center'
  }
});
